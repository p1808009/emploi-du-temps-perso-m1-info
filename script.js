function deleteFilteredEvents() {
  function generateEventKey(event) {
    // Génére une clé unique basée sur le titre, l'heure de début et de fin pour faciliter la comparaison
    return `${event.getTitle()}|${event.getStartTime().getTime()}|${event
      .getEndTime()
      .getTime()}`;
  }

  function addEvent(calendar, event) {
    calendar.createEvent(
      event.getTitle(),
      event.getStartTime(),
      event.getEndTime(),
      {
        description: event.getDescription(),
        location: event.getLocation(),
      }
    );
  }

  // TODO: Mettre à jour les variables suivantes
  const fromDate = new Date(2024, 8, 1); // Date de début
  const toDate = new Date(2025, 1, 31); // Date de fin
  const calendarId = "calendarId@import.calendar.google.com"; // Id du calendrier source
  const keywords = ["MIF18", "MIF25", "Grpe A", "Grpe C", "Grpe D", "ALT"]; // Matières et groupes à exclure
  const nameCalendar = "Mes Cours"; // Nom du calendrier de destination

  // Récupère les événements du calendrier source
  const sourceCalendar = CalendarApp.getCalendarById(calendarId);
  const originalEvents = sourceCalendar.getEvents(fromDate, toDate);

  // Filtre les événements indésirables (basé sur les mots-clés)
  const filteredOriginalEvents = originalEvents.filter((event) => {
    return !keywords.some((keyword) =>
      event.getTitle().toLowerCase().includes(keyword.toLowerCase())
    );
  });

  // Crée un ensemble de clés d'événements uniques (accélère la recherche)
  const originalEventKeys = new Set(
    filteredOriginalEvents.map((event) => generateEventKey(event))
  );

  // Récupère ou crée le calendrier de destination (si inexistant)
  let myCalendar = CalendarApp.getCalendarsByName(nameCalendar);
  if (myCalendar.length === 0) {
    myCalendar = CalendarApp.createCalendar(nameCalendar);
  } else {
    myCalendar = myCalendar[0];
  }

  // Récupère les événements du calendrier de destination
  const myEvents = myCalendar.getEvents(fromDate, toDate);

  // Crée un ensemble de clés d'événements du calendrier de destination (accélère la recherche)
  const myEventKeys = new Set(myEvents.map((event) => generateEventKey(event)));

  // Ajoute les événements manquants du calendrier source au calendrier cible
  filteredOriginalEvents.forEach((event) => {
    const eventKey = generateEventKey(event);
    if (!myEventKeys.has(eventKey)) {
      addEvent(myCalendar, event);
    }
  });

  // Supprime les événements du calendrier cible qui ne sont plus dans le calendrier source
  myEvents.forEach((event) => {
    const eventKey = generateEventKey(event);
    if (!originalEventKeys.has(eventKey)) {
      console.log(`Deleting event: ${event.getTitle()}`);
      event.deleteEvent();
    }
  });
}
