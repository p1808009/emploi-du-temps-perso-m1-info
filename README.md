# Personnaliser Votre Emploi du Temps ADE dans Google Agenda

## Vue d'ensemble

Ce tutoriel vous permettra de :

1. Récupérer l'URL de l'emploi du temps ADE.
2. Transférer cet emploi du temps dans Google Agenda.
3. Utiliser un script personnalisé pour filtrer les cours que vous ne souhaitez pas voir dans votre planning.

### Étape 1 : Récupérer l'URL de l'emploi du temps ADE

1. **Accédez à votre emploi du temps ADE** ([lien pour les M1 info](https://adelb.univ-lyon1.fr/direct/index.jsp?projectId=1&ShowPianoWeeks=true&displayConfName=DOUA_CHBIO&showTree=false&resources=9767&days=0,1,2,3,4)).
2. **Générez l'URL du calendrier** :
   - Recherchez l'option `Exporter Agenda` (icône calendrier avec une flèche).
   - Sélectionnez la date de début et la date de fin.
   - Cliquez sur `Générer URL` et copiez l'URL obtenue.

### Étape 2 : Ajouter l'emploi du temps ADE à votre Google Agenda

1. Ouvrez [Google Agenda](https://calendar.google.com).
2. Accédez à `Paramètres` > `Ajouter un agenda` > `À partir de l'URL`.
3. Collez l'URL récupérée depuis ADE.

### Étape 3 : Filtrer les événements non désirés avec un script

1. **Obtenir l’identifiant du calendrier ADE** :

   - Dans Google Agenda, trouvez le calendrier que vous venez d’ajouter.
   - Cliquez sur les trois points à côté du nom du calendrier.
   - Sélectionnez `Paramètres`.
   - Descendez jusqu’à la section `Identifiant de calendrier` et copiez l’**ID**.

2. **Exécuter le script** :

   - Ouvrez [Google Apps Script](https://script.google.com).
   - Créez un `nouveau projet`.
   - Collez le script fourni dans le dépôt Git.

3. **Personnaliser le script** : Modifiez les lignes suivantes comme suit :

   ```js
   // TODO: Mettre à jour les variables suivantes
   const fromDate = new Date(2024, 8, 1); // Date de début
   const toDate = new Date(2025, 1, 31); // Date de fin
   const calendarId = "calendarId@import.calendar.google.com"; // ID du calendrier importé d'ADE
   const keywords = ["MIF18", "MIF25", "Grpe A", "Grpe C", "Grpe D", "ALT"]; // Matières et groupes à exclure
   const nameCalendar = "Mes Cours"; // Nom du calendrier de destination
   ```

4. **Exécuter le script** :

   - Enregistrez et exécutez le script.
   - Google vous demandera d’autoriser l’accès à votre agenda — acceptez les permissions nécessaires.
   - Le script créera alors un nouveau calendrier avec les événements filtrés.

5. **(Optionnel) Déclencher le script automatiquement** :
   - Cliquez sur `Déclencheurs` (icône horloge sur la barre latérale gauche).
   - Cliquez sur `Ajouter un déclencheur`.
   - Sélectionnez `Déclencheur horaire` et définissez à quelle fréquence vous souhaitez que le script s'exécute (par exemple, quotidiennement, entre 19h et 20h).
